ceilometer-instance-poller (1.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090400).

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 09:11:42 +0100

ceilometer-instance-poller (1.0.0-1) unstable; urgency=medium

  * New upstream release:
    - always mount disks with format='raw' so it works with newer libguestfs.
    - Understand multiple AZ and cross-AZ attached volumes.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Oct 2024 14:29:33 +0100

ceilometer-instance-poller (0.1.10-1) unstable; urgency=medium

  * New upstream release:
    - More doc (in README.md).
    - Use os_family instead of os_type, as os_type is conflicting with another
      value already in instances.
    - Also reports VCPU num.
    - rename `ceilometer.instance.os.billable` to
      `ceilometer.instance.os.metadata`.
  * Cleans correctly (Closes: #1043859).

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 Apr 2023 09:37:57 +0200

ceilometer-instance-poller (0.1.5-1) unstable; urgency=medium

  * New upstream release (fixes minor typo in the json output).

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Mar 2023 17:03:43 +0100

ceilometer-instance-poller (0.1.4-2) unstable; urgency=medium

  * Re-upload source-only.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Feb 2023 23:54:17 +0100

ceilometer-instance-poller (0.1.4-1) unstable; urgency=medium

  * Initial release (Closes: #1029545).

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Jan 2023 10:49:31 +0100
