# Table of Contents

- [What is ceilometer-instance-poller](#what-is-ceilometer-instance-poller)
  * [General description](#general-description)
  * [Values being polled](#values-being-polled)
  * [What for](#what-for)
- [How to setup](#how-to-setup)
  * [Requires ceilometer patches](#requires-ceilometer-patches)
  * [Configuring ceilometer on the compute nodes](#configuring-ceilometer-on-the-compute-nodes)
  * [Sending metadata to Gnocchi](#sending-metadata-to-Gnocchi)
  * [Altering attributes in Gnocchi](#altering-attributes-in-gnocchi)
  * [Cloudkitty](#cloudkitty)

# What is ceilometer-instance-poller

### General description

This Python package uses libvirt and libguestfs to poll data otherwise not
available through a standard Ceilometer installation on a compute node.
/usr/bin/ceilometer-instance-poller produces a json output that can be
used by Ceilometer as a compute dynamic pollster.

This relies on this patch being merged:

https://review.opendev.org/c/openstack/ceilometer/+/852021

At the time of writing, this patch is available in Debian from the
Victoria release, but in other distributions, it may only appear
after Antelope.

### Values being polled

All the metadata needed to bill OS licenses of your running VMs.

Example output:

```
[
  {
    "resource_id": "190f5907-fd86-4ade-9315-705e14aa63cd",
    "vcpus": 4
    "os_family": "windows",
    "os_distro": "windows",
    "os_info": "win2k16",
    "os_version_major": 10,
    "os_version_minor": 0,
    "os_product_name": "Windows Server 2022 Datacenter Evaluation",
    "os_product_variant": "Server"
  },
  {
    "resource_id": "9e92c509-0b48-43d0-a552-42de6f001e36",
    "vcpus": 1
    "os_family": "linux",
    "os_distro": "debian",
    "os_info": "debian11",
    "os_version_major": 11,
    "os_version_minor": 2,
    "os_product_name": "11.2",
    "os_product_variant": "unknown"
  }
]
```

### What for

Some operating systems aren't free, and needs to be charged.
Though if booted from volume, there's no way to know what the
base image was, and if your customer is running Windows. That's
what this script does: it looks into the running VMs using
read-only libguestfs to know if your customer needs to be
charged for using a Windows license.

# How to setup
## Requires ceilometer patches

tl;dr: Only OpenStack Antelope has all the Ceilometer patches,
otherwise you will need backports.

Depending on the version of Ceilometer running in the OpenStack
deployment where you need ceilometer-instance-poller, you may
need some extra Ceilometer patches, backported to your version
of OpenStack. The Debian team already maintains versions of these
patches backported to all versions of Ceilometer from Victoria
to Zed. A full list of these patches is available over here:

https://salsa.debian.org/openstack-team/services/ceilometer/-/tree/debian/victoria/debian/patches

So, basically:
- Introduce_timeout_in_dynamic_pollsters_sub-system.patch
- OpenStack_Dynamic_pollsters_metadata_enrichment_with_other_OpenStack_APIs_data.patch
- Add_response_handlers_to_support_different_response_types.patch
- Add_support_to_namespaces_on_dynamic_pollsters.patch
- Add_support_to_host_command_dynamic_pollster_definitions.patch

## Configuring ceilometer on the compute nodes

The new pollster must be in the folder `/etc/ceilometer/pollsters.d` and its
name must be referenced in `/etc/ceilometer/polling.yaml`. Here's an example
`ceilometer_instance_poller.yaml` configuration in the `pollsters.d`:

```
---
- name: "compute.instance.os.metadata"
  sample_type: "gauge"
  unit: "vcpu"
  value_attribute: "vcpus"
  resource_id_attribute: "resource_id"
  host_command: "sudo /usr/bin/ceilometer-instance-poller --config-file /etc/ceilometer-instance-poller/ceilometer-instance-poller.conf"
  namespaces: "compute"
  metadata_fields:
    - "os_family"
    - "os_distro"
    - "os_info"
    - "os_product_name"
    - "os_product_variant"
    - "os_version_major"
    - "os_version_minor"
  response_handlers:
    - json
```

Here we retreive vcpus number for per-cpu licencing model. You may replace 'value_attribute' by another field and use 'metadata_mapping' / 'default_value' if you prefer a different binding. 

## Sending metadata to Gnocchi

It's not needed to send all of the metadata, one may decide to send only the
feilds they need. For example, in `/etc/ceilometer/gnocchi_resources.yaml`:

```
...
  - resource_type: instance
    metrics:
    attributes:
      os_family: resource_metadata.os_family
      os_distro: resource_metadata.os_distro
      os_info: resource_metadata.os_info
```

## Altering attributes in Gnocchi

```
$ openstack metric resource-type update instance -a os_family:string:false:max_length=255
$ openstack metric resource-type update instance -a os_distro:string:false:max_length=255
$ openstack metric resource-type update instance -a os_info:string:false:max_length=255
```

## Cloudkitty

Here's a `metrics.yaml` that matches the above:

```
  compute.instance.os.metadata:
    unit: instance
    alt_name: software_licence
    groupby:
      - project_id
    metadata:
      - os_family
      - os_distro
      - os_info
    extra_args:
      aggregation_method: mean
      resource_type: instance
      force_granularity: 3600
```

After restarting the services, we normally see the metric under alt_name : software_licence.

```
$ openstack rating info metric get software_licence
+------------------+----------+---------------------------------------+
| Metric           | Unit     | Metadata                              |
+------------------+----------+---------------------------------------+
| software_licence | instance | ['os_distro', 'os_family', 'os_info'] |
+------------------+----------+---------------------------------------+
```

Then let's add a new Cloudkitty group:

```
$ openstack rating hashmap group create software_licences
+-------------------+--------------------------------------+
| Name              | Group ID                             |
+-------------------+--------------------------------------+
| software_licences | e3d5ba8b-86af-4403-86fa-1a540295c68a |
+-------------------+--------------------------------------+
```

Then the service. It must have the same name as the (renamed) metric: software_licence.

```
$ openstack rating hashmap service create software_licence
+------------------+--------------------------------------+
| Name             | Service ID                           |
+------------------+--------------------------------------+
| software_licence | 89ce2fe5-ab99-4818-a4ea-040ab9e5d20c |
+------------------+--------------------------------------+
```

We then add new fields:

```
$ openstack rating hashmap field create 89ce2fe5-ab99-4818-a4ea-040ab9e5d20c os_family
$ openstack rating hashmap field create 89ce2fe5-ab99-4818-a4ea-040ab9e5d20c os_info
$ openstack rating hashmap field create 89ce2fe5-ab99-4818-a4ea-040ab9e5d20c os_distro
$ openstack rating hashmap field list 89ce2fe5-ab99-4818-a4ea-040ab9e5d20c
+-----------+--------------------------------------+--------------------------------------+
| Name      | Field ID                             | Service ID                           |
+-----------+--------------------------------------+--------------------------------------+
| os_distro | 21a40fe1-f896-4540-b484-0b7bd5f414ab | 89ce2fe5-ab99-4818-a4ea-040ab9e5d20c |
| os_info   | 23ddbff4-a8fe-4e1a-9526-c8bac8e13641 | 89ce2fe5-ab99-4818-a4ea-040ab9e5d20c |
| os_family | 5140ebd2-b071-41b9-81f3-2c69184bc45a | 89ce2fe5-ab99-4818-a4ea-040ab9e5d20c |
+-----------+--------------------------------------+--------------------------------------+
```

And a new mapping:

```
$ openstack rating hashmap mapping create --group-id e3d5ba8b-86af-4403-86fa-1a540295c68a --field-id 23ddbff4-a8fe-4e1a-9526-c8bac8e13641 -t flat --value win2k16 0.5
+--------------------------------------+---------+------------+------+--------------------------------------+------------+--------------------------------------+------------+
| Mapping ID                           | Value   | Cost       | Type | Field ID                             | Service ID | Group ID                             | Project ID |
+--------------------------------------+---------+------------+------+--------------------------------------+------------+--------------------------------------+------------+
| d0e7877a-256f-4566-9f3c-6356b1edc987 | win2k16 | 0.05000000 | flat | 23ddbff4-a8fe-4e1a-9526-c8bac8e13641 | None       | e3d5ba8b-86af-4403-86fa-1a540295c68a | None       |
+--------------------------------------+---------+------------+------+--------------------------------------+------------+--------------------------------------+------------+
```
